package hello.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
	List<Customer> findByGroupIdAndLoginId(String groupId, String loginId);
	boolean existsByGroupId(String groupId);
}