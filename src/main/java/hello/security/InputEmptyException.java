package hello.security;

import org.springframework.security.core.AuthenticationException;

public class InputEmptyException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public InputEmptyException(String msg) {
		super(msg);
	}

}
