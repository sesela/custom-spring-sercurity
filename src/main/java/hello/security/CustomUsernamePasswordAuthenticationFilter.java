package hello.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private static final Logger log = LoggerFactory.getLogger(CustomUsernamePasswordAuthenticationFilter.class);

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		String groupId = request.getParameter("groupId");
		String loginId = request.getParameter("loginId");
		String password = obtainPassword(request);
		if (StringUtils.isEmpty(groupId) || StringUtils.isEmpty(loginId)  || StringUtils.isEmpty(password)) {
			throw new InputEmptyException("input empty");
		}
		String groupIdAndUserId = groupId.trim() + " " + loginId.trim();
		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
				groupIdAndUserId, password);
		setDetails(request, authRequest);
		log.info("groupId: " + groupId + ", loginId: " + loginId + ", password: " + password);
		return this.getAuthenticationManager().authenticate(authRequest);
	}
}
