package hello.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

public class CustomSimpleUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	private static final Logger log = LoggerFactory.getLogger(CustomSimpleUrlAuthenticationFailureHandler.class);

	public CustomSimpleUrlAuthenticationFailureHandler(String failureUrl) {
		super(failureUrl);
	}
	
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		if (exception instanceof InputEmptyException) {
			saveException(request, exception);
			getRedirectStrategy().sendRedirect(request, response, WebSecurityConfig.LOGIN_URL + "?empty");
		} else if (exception instanceof GroupIdNotFoundException) {
			saveException(request, exception);
			getRedirectStrategy().sendRedirect(request, response, WebSecurityConfig.LOGIN_URL + "?invalid_group");
		} else {
			super.onAuthenticationFailure(request, response, exception);
		}
		log.info("Exception occured", exception);
	}

	
}
