package hello.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import hello.domain.Customer;
import hello.domain.CustomerRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String LOGIN_URL = "/customlogin";

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.addFilterAt(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers("/", "/home", LOGIN_URL).permitAll()
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage(LOGIN_URL)
			// https://stackoverflow.com/questions/43838639/spring-security-custom-authentication-failure-handler-redirect-with-parameter
//				.failureHandler(new CustomSimpleUrlAuthenticationFailureHandler(LOGIN_URL + "?error"))
			.permitAll()
			.and()
		.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//			.logoutSuccessUrl("/") // ログアウト成功時の遷移先指定
			.permitAll().deleteCookies("JSESSIONID")
			.and()
		.sessionManagement().invalidSessionUrl("/");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, CustomerRepository customerRepository) throws Exception {
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setUserDetailsService(userDetailsService(customerRepository));
		daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
		daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
		auth.authenticationProvider(daoAuthenticationProvider);
	}

	private CustomUsernamePasswordAuthenticationFilter authenticationFilter() throws Exception {
		CustomUsernamePasswordAuthenticationFilter filter = new CustomUsernamePasswordAuthenticationFilter();
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setAuthenticationFailureHandler(new CustomSimpleUrlAuthenticationFailureHandler(LOGIN_URL + "?error"));
		return filter;
	}
	
	private UserDetailsService userDetailsService(CustomerRepository customerRepository) {
//		return (groupIdAndloginId) -> {
//			UserBuilder builder = null;
//			String[] groupIdAndloginIdArray = groupIdAndloginId.split(" ");
//			if (groupIdAndloginIdArray.length != 2) {
//				throw new UsernameNotFoundException("invalid input groupId or loginId");
//			}
//			String groupId = groupIdAndloginIdArray[0];
//			String loginId = groupIdAndloginIdArray[1];
//
//			if (!customerRepository.existsByGroupId(groupId)) {
//				throw new GroupIdNotFoundException("not found groupId: " + groupId);
//			}
//			List<Customer> customerList = customerRepository.findByGroupIdAndLoginId(groupId, loginId);
//			if (customerList.size() != 1) {
//				throw new UsernameNotFoundException("not found user: " + groupId + ":" + loginId);
//			}
//			builder = User.withUsername(loginId);
//			builder.password(customerList.get(0).getPassword());
//			builder.roles("NORMAL");
//			return builder.build();
//		};

		return (groupIdAndloginId) -> {
			String groupId = null;
			String loginId = null;
			String[] groupIdAndloginIdArray = groupIdAndloginId.split(" ");
			if (groupIdAndloginIdArray.length == 2) {
				groupId = groupIdAndloginIdArray[0];
				loginId = groupIdAndloginIdArray[1];
			}
			if (!customerRepository.existsByGroupId(groupId)) {
				throw new GroupIdNotFoundException("not found groupId: " + groupId);
			}
			List<Customer> customerList = customerRepository.findByGroupIdAndLoginId(groupId, loginId);
			if (customerList.size() != 1) {
				throw new UsernameNotFoundException("not found user: " + groupId + ":" + loginId);
			}
			return new CustomUserDetails(groupId, loginId, customerList.get(0).getPassword(), Arrays.asList(new SimpleGrantedAuthority("ROLE_NORMAL")));
		};

	}
}