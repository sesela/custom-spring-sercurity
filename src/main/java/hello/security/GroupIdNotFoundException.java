package hello.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class GroupIdNotFoundException extends UsernameNotFoundException {

	private static final long serialVersionUID = 1L;

	public GroupIdNotFoundException(String msg) {
		super(msg);
	}

}
